# Create Study Plan

This document will guide you through how to import and setup a calendar using [Notion](https://www.notion.so) so that you can create a study plan for the [Security Blue Team's](https://securityblue.team/) certifications.

This walkthrough assumes that you already have an account with Notion, if not, please sign up before continuing.

![Properties Set](./images/study_plan-info.png)


#### Table of Contents
1. [Downloading and Importing CSV](#downloading-and-importing-csv)
    * [Download CSV](#1-download-the-csv)
    * [Importing CSV](#2-importing-the-csv)
2. [Creating the Calendar View](#creating-the-calendar-view)
    * [Adding the Calendar View](#adding-the-calendar-view)
    * [Setting the Last Day of Access](#setting-the-last-day-of-access)
    * [Adding More Information to the Calendar](#adding-more-information-to-the-calendar)


---
### Downloading and Importing CSV

#### 1. Download the CSV
Open up a web browser of your choice and go to: https://gitlab.com/scyber-security/security-blue-team/-/blob/main/study_plans/

Download the CSV file which relates to the BTL level you're tackling.

Once this page loads, you should see the CSV file which we'll download and import into Notion. To download the file click the icon on the far right as shown circled in the picture below:

![Download Study Plan](./images/study_plan-download_csv.png)


#### 2. Importing the CSV
Now that you have the CSV saved to your local disk, we can import it into Notion. Find the `Import` button on the right side (towards the bottom).

![Import Button](./images/study_plan-import_button.png)

Clicking on that, will give you a box where you can upload the CSV.

![Import CSV](./images/study_plan-import_csv.png)

Upload the CSV file and wait for it to import. After it's finished importing, you should have something which looks like this:

![CSV Imported](./images/study_plan-imported.png)


#### 3. Fixing Import Issues
Once you've imported the CSV file, the `Due Date` field may not be set to the correct porperty type. To fix this, simply click on the `Due Date` cell, select `Text` under `Property Type`, and then click the `Date` option.

![Due Date Fix](./images/study_plan-due_date_fix.png)


### Creating the Calendar View

#### Adding the Calendar View
In Notion, select the `+ Add View` button underneath the title.

![Add View Button](./images/study_plan-add_view_button.png)

Then select the `Calendar` button and click `Create`.

![Create the Calendar](./images/study_plan-create_calendar.png)

This will create you a calendar view, which will be empty at the moment as you haven't set any due dates yet.


#### Setting the Last Day of Access

When studying for the certifications, you need to make sure that you take your exam before you lose access to the material. We can easily mark this date on the calendar by finding the `LAST DAY OF ACCESS` in the table, and setting the `Due Date`.

For example, at the time of writing this, I had **106 days** left of access. This information can be found once you open up the course.

![Days Left](./images/study_plan-expires_in.png)

A quick query in your search engine of choice using `x days from today` will give you the last day of access.

![Days From Now](./images/study_plan-days_from_now.png)

Going back to Notion, we can now enter the date for the `LAST DAY OF ACCESS`. Back in the `Table View`, simply click on the cell for the `Due Date`, which should open up a calendar, find the date and click it.

![Selecting the Date](./images/study_plan-expires_date_set.png)
![Expires Date Set](./images/study_plan-expires_date.png)

At this point you can also start to set the `Due Dates` for all of the lessons.


#### Adding More Information to the Calendar
After setting the `Due Dates`, you'll start to see the calendar view will start to populate with the lessons.

![Limited Calendar](./images/study_plan-no_info.png)

If we want to be able to quickly check off the lesson as done, or see any other information to do with the task, we're going to need to change the properties.

In the Calendar view, click on the three dots next to the `New` button, and click the `Properties` button to bring up the `Properties` menu. From here you can toggle which properties you would like to have visible on the calendar.

![Properties Button](./images/study_plan-properties_button.png)
![Properties Menu](./images/study_plan-properties_set.png)
![Properties Set](./images/study_plan-info.png)
