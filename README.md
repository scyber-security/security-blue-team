# Security Blue Team

I created this repo to help share information to help people pass their **Blue Team Level *x*** certification.

Whilst I'm still going through it myself, the first thing I wanted to share is how to create an effective study plan using Notion. Luckily for you, I've done most of the hard work, and you just need to follow the steps in https://gitlab.com/scyber-security/security-blue-team/-/blob/main/Create_Study_Plan.md.
